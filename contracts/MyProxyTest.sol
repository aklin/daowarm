// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

struct MyTestData {
    bytes32 value;
    bytes32 dataType;
    bytes32 dataDescr;
}

struct MyTestData2 {
    bytes32 value;
    bytes32 dataType;
    bytes32 dataDescr;
}

contract MyProxyTest {
    uint256[10] __reserve;
    string public version;
    mapping (bytes32 => MyTestData) public items;
    mapping (bytes32 => MyTestData2) public items2;

    event ItemSet(bytes32 key, MyTestData data);
    event Item2Set(bytes32 key, MyTestData2 data);

    function setVersion(string memory value) external {
        version = value;
    }

    function setItem(bytes32 key, MyTestData memory data) external {
        items[key] = data;
        emit ItemSet(key, data);
    }

    function setItem2(bytes32 key, MyTestData2 memory data) external {
        items2[key] = data;
        emit Item2Set(key, data);
    }
}
