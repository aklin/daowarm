// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package bindings

import (
	"errors"
	"math/big"
	"strings"

	"gitlab.com/aklin/linkch"
	"gitlab.com/aklin/linkch/accounts/abi"
	"gitlab.com/aklin/linkch/accounts/abi/bind"
	"gitlab.com/aklin/linkch/common"
	"gitlab.com/aklin/linkch/core/types"
	"gitlab.com/aklin/linkch/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
	_ = abi.ConvertType
)

// TemplateMetaData contains all meta data concerning the Template contract.
var TemplateMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"string\",\"name\":\"_version\",\"type\":\"string\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"},{\"indexed\":false,\"internalType\":\"bytes32\",\"name\":\"value\",\"type\":\"bytes32\"}],\"name\":\"ItemSet\",\"type\":\"event\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"}],\"name\":\"items\",\"outputs\":[{\"internalType\":\"bytes32\",\"name\":\"\",\"type\":\"bytes32\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes32\",\"name\":\"key\",\"type\":\"bytes32\"},{\"internalType\":\"bytes32\",\"name\":\"value\",\"type\":\"bytes32\"}],\"name\":\"setItem\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"version\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"}]",
	Bin: "0x608060405234801561001057600080fd5b5060405161072138038061072183398181016040528101906100329190610270565b806000908051906020019061004892919061004f565b505061031a565b82805461005b906102e8565b90600052602060002090601f01602090048101928261007d57600085556100c4565b82601f1061009657805160ff19168380011785556100c4565b828001600101855582156100c4579182015b828111156100c35782518255916020019190600101906100a8565b5b5090506100d191906100d5565b5090565b5b808211156100ee5760008160009055506001016100d6565b5090565b6000604051905090565b600080fd5b600080fd5b600080fd5b600080fd5b6000601f19601f8301169050919050565b7f4e487b7100000000000000000000000000000000000000000000000000000000600052604160045260246000fd5b61015982610110565b810181811067ffffffffffffffff8211171561017857610177610121565b5b80604052505050565b600061018b6100f2565b90506101978282610150565b919050565b600067ffffffffffffffff8211156101b7576101b6610121565b5b6101c082610110565b9050602081019050919050565b60005b838110156101eb5780820151818401526020810190506101d0565b838111156101fa576000848401525b50505050565b600061021361020e8461019c565b610181565b90508281526020810184848401111561022f5761022e61010b565b5b61023a8482856101cd565b509392505050565b600082601f83011261025757610256610106565b5b8151610267848260208601610200565b91505092915050565b600060208284031215610286576102856100fc565b5b600082015167ffffffffffffffff8111156102a4576102a3610101565b5b6102b084828501610242565b91505092915050565b7f4e487b7100000000000000000000000000000000000000000000000000000000600052602260045260246000fd5b6000600282049050600182168061030057607f821691505b60208210811415610314576103136102b9565b5b50919050565b6103f8806103296000396000f3fe608060405234801561001057600080fd5b50600436106100415760003560e01c806348f343f31461004657806354fd4d5014610076578063f56256c714610094575b600080fd5b610060600480360381019061005b91906101e6565b6100b0565b60405161006d9190610222565b60405180910390f35b61007e6100c8565b60405161008b91906102d6565b60405180910390f35b6100ae60048036038101906100a991906102f8565b610156565b005b60016020528060005260406000206000915090505481565b600080546100d590610367565b80601f016020809104026020016040519081016040528092919081815260200182805461010190610367565b801561014e5780601f106101235761010080835404028352916020019161014e565b820191906000526020600020905b81548152906001019060200180831161013157829003601f168201915b505050505081565b8060016000848152602001908152602001600020819055507fe79e73da417710ae99aa2088575580a60415d359acfad9cdd3382d59c80281d4828260405161019f929190610399565b60405180910390a15050565b600080fd5b6000819050919050565b6101c3816101b0565b81146101ce57600080fd5b50565b6000813590506101e0816101ba565b92915050565b6000602082840312156101fc576101fb6101ab565b5b600061020a848285016101d1565b91505092915050565b61021c816101b0565b82525050565b60006020820190506102376000830184610213565b92915050565b600081519050919050565b600082825260208201905092915050565b60005b8381101561027757808201518184015260208101905061025c565b83811115610286576000848401525b50505050565b6000601f19601f8301169050919050565b60006102a88261023d565b6102b28185610248565b93506102c2818560208601610259565b6102cb8161028c565b840191505092915050565b600060208201905081810360008301526102f0818461029d565b905092915050565b6000806040838503121561030f5761030e6101ab565b5b600061031d858286016101d1565b925050602061032e858286016101d1565b9150509250929050565b7f4e487b7100000000000000000000000000000000000000000000000000000000600052602260045260246000fd5b6000600282049050600182168061037f57607f821691505b6020821081141561039357610392610338565b5b50919050565b60006040820190506103ae6000830185610213565b6103bb6020830184610213565b939250505056fea264697066735822122001ed92b955db1f3f7c3e0857c723f495cc9050f892b66880efbe68aa31bd61bd64736f6c63430008090033",
}

// TemplateABI is the input ABI used to generate the binding from.
// Deprecated: Use TemplateMetaData.ABI instead.
var TemplateABI = TemplateMetaData.ABI

// TemplateBin is the compiled bytecode used for deploying new contracts.
// Deprecated: Use TemplateMetaData.Bin instead.
var TemplateBin = TemplateMetaData.Bin

// DeployTemplate deploys a new Ethereum contract, binding an instance of Template to it.
func DeployTemplate(auth *bind.TransactOpts, backend bind.ContractBackend, _version string) (common.Address, *types.Transaction, *Template, error) {
	parsed, err := TemplateMetaData.GetAbi()
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	if parsed == nil {
		return common.Address{}, nil, nil, errors.New("GetABI returned nil")
	}

	address, tx, contract, err := bind.DeployContract(auth, *parsed, common.FromHex(TemplateBin), backend, _version)
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	return address, tx, &Template{TemplateCaller: TemplateCaller{contract: contract}, TemplateTransactor: TemplateTransactor{contract: contract}, TemplateFilterer: TemplateFilterer{contract: contract}}, nil
}

// DeployTemplateSync deploys a new Ethereum contract and waits for receipt, binding an instance of TemplateSession to it.
func DeployTemplateSync(session *bind.TransactSession, backend bind.ContractBackend, _version string) (*types.Transaction, *types.Receipt, *TemplateSession, error) {
	parsed, err := abi.JSON(strings.NewReader(TemplateABI))
	if err != nil {
		return nil, nil, nil, err
	}
	session.Lock()
	address, tx, _, err := bind.DeployContract(session.TransactOpts, parsed, common.FromHex(TemplateBin), backend, _version)
	if err != nil {
		session.Unlock()
		return nil, nil, nil, err
	}
	receipt, err := session.WaitTransaction(tx)
	if err != nil {
		session.Unlock()
		return nil, nil, nil, err
	}
	session.TransactOpts.Nonce.Add(session.TransactOpts.Nonce, big.NewInt(1))
	session.Unlock()
	contractSession, err := NewTemplateSession(address, backend, session)
	return tx, receipt, contractSession, err
}

// Template is an auto generated Go binding around an Ethereum contract.
type Template struct {
	TemplateCaller     // Read-only binding to the contract
	TemplateTransactor // Write-only binding to the contract
	TemplateFilterer   // Log filterer for contract events
}

// TemplateCaller is an auto generated read-only Go binding around an Ethereum contract.
type TemplateCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// TemplateTransactor is an auto generated write-only Go binding around an Ethereum contract.
type TemplateTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// TemplateFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type TemplateFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// TemplateSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type TemplateSession struct {
	Contract           *Template // Generic contract binding to set the session for
	transactionSession *bind.TransactSession
	Address            common.Address
}

// TemplateCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type TemplateCallerSession struct {
	Contract *TemplateCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts   // Call options to use throughout this session
}

// TemplateTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type TemplateTransactorSession struct {
	Contract     *TemplateTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts   // Transaction auth options to use throughout this session
}

// TemplateRaw is an auto generated low-level Go binding around an Ethereum contract.
type TemplateRaw struct {
	Contract *Template // Generic contract binding to access the raw methods on
}

// TemplateCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type TemplateCallerRaw struct {
	Contract *TemplateCaller // Generic read-only contract binding to access the raw methods on
}

// TemplateTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type TemplateTransactorRaw struct {
	Contract *TemplateTransactor // Generic write-only contract binding to access the raw methods on
}

// NewTemplate creates a new instance of Template, bound to a specific deployed contract.
func NewTemplate(address common.Address, backend bind.ContractBackend) (*Template, error) {
	contract, err := bindTemplate(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Template{TemplateCaller: TemplateCaller{contract: contract}, TemplateTransactor: TemplateTransactor{contract: contract}, TemplateFilterer: TemplateFilterer{contract: contract}}, nil
}

// NewTemplateCaller creates a new read-only instance of Template, bound to a specific deployed contract.
func NewTemplateCaller(address common.Address, caller bind.ContractCaller) (*TemplateCaller, error) {
	contract, err := bindTemplate(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &TemplateCaller{contract: contract}, nil
}

// NewTemplateTransactor creates a new write-only instance of Template, bound to a specific deployed contract.
func NewTemplateTransactor(address common.Address, transactor bind.ContractTransactor) (*TemplateTransactor, error) {
	contract, err := bindTemplate(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &TemplateTransactor{contract: contract}, nil
}

// NewTemplateFilterer creates a new log filterer instance of Template, bound to a specific deployed contract.
func NewTemplateFilterer(address common.Address, filterer bind.ContractFilterer) (*TemplateFilterer, error) {
	contract, err := bindTemplate(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &TemplateFilterer{contract: contract}, nil
}

func NewTemplateSession(address common.Address, backend bind.ContractBackend, transactionSession *bind.TransactSession) (*TemplateSession, error) {
	TemplateInstance, err := NewTemplate(address, backend)
	if err != nil {
		return nil, err
	}
	return &TemplateSession{
		Contract:           TemplateInstance,
		transactionSession: transactionSession,
		Address:            address,
	}, nil
}

// bindTemplate binds a generic wrapper to an already deployed contract.
func bindTemplate(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := TemplateMetaData.GetAbi()
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, *parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Template *TemplateRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Template.Contract.TemplateCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Template *TemplateRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Template.Contract.TemplateTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Template *TemplateRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Template.Contract.TemplateTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Template *TemplateCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Template.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Template *TemplateTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Template.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Template *TemplateTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Template.Contract.contract.Transact(opts, method, params...)
}

// Items is a free data retrieval call binding the contract method 0x48f343f3.
//
// Solidity: function items(bytes32 ) view returns(bytes32)
func (_Template *TemplateCaller) Items(opts *bind.CallOpts, arg0 [32]byte) ([32]byte, error) {
	var out []interface{}
	err := _Template.contract.Call(opts, &out, "items", arg0)

	if err != nil {
		return *new([32]byte), err
	}

	out0 := *abi.ConvertType(out[0], new([32]byte)).(*[32]byte)

	return out0, err

}

// Items is a free data retrieval call binding the contract method 0x48f343f3.
//
// Solidity: function items(bytes32 ) view returns(bytes32)
func (_Template *TemplateSession) Items(arg0 [32]byte) ([32]byte, error) {
	return _Template.Contract.Items(_Template.transactionSession.CallOpts, arg0)
}

// Items is a free data retrieval call binding the contract method 0x48f343f3.
//
// Solidity: function items(bytes32 ) view returns(bytes32)
func (_Template *TemplateCallerSession) Items(arg0 [32]byte) ([32]byte, error) {
	return _Template.Contract.Items(&_Template.CallOpts, arg0)
}

// Version is a free data retrieval call binding the contract method 0x54fd4d50.
//
// Solidity: function version() view returns(string)
func (_Template *TemplateCaller) Version(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _Template.contract.Call(opts, &out, "version")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// Version is a free data retrieval call binding the contract method 0x54fd4d50.
//
// Solidity: function version() view returns(string)
func (_Template *TemplateSession) Version() (string, error) {
	return _Template.Contract.Version(_Template.transactionSession.CallOpts)
}

// Version is a free data retrieval call binding the contract method 0x54fd4d50.
//
// Solidity: function version() view returns(string)
func (_Template *TemplateCallerSession) Version() (string, error) {
	return _Template.Contract.Version(&_Template.CallOpts)
}

// SetItem is a paid mutator transaction binding the contract method 0xf56256c7.
//
// Solidity: function setItem(bytes32 key, bytes32 value) returns()
func (_Template *TemplateTransactor) SetItem(opts *bind.TransactOpts, key [32]byte, value [32]byte) (*types.Transaction, error) {
	return _Template.contract.Transact(opts, "setItem", key, value)
}

// SetItem is a paid mutator transaction binding the contract method 0xf56256c7.
//
// Solidity: function setItem(bytes32 key, bytes32 value) returns()
func (_Template *TemplateSession) SetItem(key [32]byte, value [32]byte) (*types.Transaction, *types.Receipt, error) {
	_Template.transactionSession.Lock()
	tx, err := _Template.Contract.SetItem(_Template.transactionSession.TransactOpts, key, value)
	if err != nil {
		_Template.transactionSession.Unlock()
		return nil, nil, err
	}
	_Template.transactionSession.TransactOpts.Nonce.Add(_Template.transactionSession.TransactOpts.Nonce, big.NewInt(1))
	_Template.transactionSession.Unlock()
	receipt, err := _Template.transactionSession.WaitTransaction(tx)
	return tx, receipt, err
}

// SetItem is a paid mutator transaction binding the contract method 0xf56256c7.
//
// Solidity: function setItem(bytes32 key, bytes32 value) returns()
func (_Template *TemplateTransactorSession) SetItem(key [32]byte, value [32]byte) (*types.Transaction, error) {
	return _Template.Contract.SetItem(&_Template.TransactOpts, key, value)
}

// TemplateItemSetIterator is returned from FilterItemSet and is used to iterate over the raw logs and unpacked data for ItemSet events raised by the Template contract.
type TemplateItemSetIterator struct {
	Event *TemplateItemSet // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *TemplateItemSetIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(TemplateItemSet)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(TemplateItemSet)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *TemplateItemSetIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *TemplateItemSetIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// TemplateItemSet represents a ItemSet event raised by the Template contract.
type TemplateItemSet struct {
	Key   [32]byte
	Value [32]byte
	Raw   types.Log // Blockchain specific contextual infos
}

// FilterItemSet is a free log retrieval operation binding the contract event 0xe79e73da417710ae99aa2088575580a60415d359acfad9cdd3382d59c80281d4.
//
// Solidity: event ItemSet(bytes32 key, bytes32 value)
func (_Template *TemplateFilterer) FilterItemSet(opts *bind.FilterOpts) (*TemplateItemSetIterator, error) {

	logs, sub, err := _Template.contract.FilterLogs(opts, "ItemSet")
	if err != nil {
		return nil, err
	}
	return &TemplateItemSetIterator{contract: _Template.contract, event: "ItemSet", logs: logs, sub: sub}, nil
}

// WatchItemSet is a free log subscription operation binding the contract event 0xe79e73da417710ae99aa2088575580a60415d359acfad9cdd3382d59c80281d4.
//
// Solidity: event ItemSet(bytes32 key, bytes32 value)
func (_Template *TemplateFilterer) WatchItemSet(opts *bind.WatchOpts, sink chan<- *TemplateItemSet) (event.Subscription, error) {

	logs, sub, err := _Template.contract.WatchLogs(opts, "ItemSet")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(TemplateItemSet)
				if err := _Template.contract.UnpackLog(event, "ItemSet", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseItemSet is a log parse operation binding the contract event 0xe79e73da417710ae99aa2088575580a60415d359acfad9cdd3382d59c80281d4.
//
// Solidity: event ItemSet(bytes32 key, bytes32 value)
func (_Template *TemplateFilterer) ParseItemSet(log types.Log) (*TemplateItemSet, error) {
	event := new(TemplateItemSet)
	if err := _Template.contract.UnpackLog(event, "ItemSet", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
