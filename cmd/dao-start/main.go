package main

import (
	"fmt"
	"github.com/urfave/cli/v2"
	"gitlab.com/aklin/daowarm/cmd/dao-start/dao/base_template"
	"gitlab.com/aklin/daowarm/cmd/dao-start/dao/store"
	"gitlab.com/aklin/daowarm/cmd/dao-start/utils"
	"os"
)

var app = cli.NewApp()

func init() {
	app.Action = runApp
	app.Name = "DAO Start 0.01"
	app.Description = "DAO Start 0.01"
	app.Copyright = "AVKLIN 2023"
	app.Flags = utils.GeneralFlags
	app.Commands = []*cli.Command{
		&base_template.Template,
		&store.Store,
	}
}

func main() {
	if err := app.Run(os.Args); err != nil {
		panic(fmt.Sprintf("Init app error: %+v", err))
	}
}

func runApp(ctx *cli.Context) error {
	return nil
}
