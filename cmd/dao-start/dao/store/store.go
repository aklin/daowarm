package store

import (
	"fmt"
	"github.com/urfave/cli/v2"
	"gitlab.com/aklin/daowarm/bindings"
	infr "gitlab.com/aklin/daowarm/cmd/dao-start/infrastructure"
	"gitlab.com/aklin/linkch/common"
	"log"
)

var (
	startFlag = cli.StringFlag{
		Name:  "start-flag",
		Usage: "Просто какой-то флаг...",
	}

	// --- Задать функцию вызова !!!
	Store = cli.Command{
		// --- Задать параметр вызова !!!
		Name:   "store",
		Usage:  "Проба пера...",
		Action: start,
		Flags:  []cli.Flag{&startFlag},
	}
)

func buildSession(ctx *cli.Context) (
	// --- Задать объект сессии [[СмК]]Session, меняем его, а дальше просто исправляем ошибки и ВСЕ !!!
	c *infr.Client, seS *bindings.FallbackSession) {
	fmt.Println("Привязываемся к клиенту...")
	c, err := infr.NewClientWithContext(ctx)
	if err != nil {
		log.Fatal("NewClientWithContext: ", err)
	}
	if false {
		fmt.Println("Деплоим СмК...")
		// --- Задать функцию деплоя Deploy[[СмК]]Sync !!!
		_, _, seS, err = bindings.DeployFallbackSync(c.TxSession, c.ETHClient) // --- Задать входные параметры деплоя !!!
	} else {
		fmt.Println("Привязываемся к СмК...")
		// --- Задать функцию привязки New[[СмК]]Session !!!
		seS, err = bindings.NewFallbackSession(
			common.HexToAddress("0x1828c132060f1e46BFE6A531C79132B520C9697f"),
			c.ETHClient, c.TxSession)
	}
	if err != nil {
		log.Fatal("NewStoreSession: ", err)
	}

	fmt.Println("Адрес контракта", seS.Address.Hex())
	return c, seS
}

func start(ctx *cli.Context) error {
	_, seS := buildSession(ctx)

	// --- Работа с элементами seS !!!

	//c.TxSession.TransactOpts.Value = infr.ToWei(0.0001, 18)
	//_, _, err := seS.Contribute()
	//if err != nil {
	//	log.Fatal("Contribute: ", err)
	//}

	//c.TxSession.TransactOpts.Value = big.NewInt(10)
	//_, err := seS.Contract.Receive(c.TxSession.TransactOpts)
	//if err != nil {
	//	log.Fatal("Receive: ", err)
	//}
	//c.TxSession.TransactOpts.Value = big.NewInt(0)

	//_, _, err := seS.Withdraw()
	//if err != nil {
	//	log.Fatal("Withdraw: ", err)
	//}

	ownerAddress, err := seS.Owner()
	if err != nil {
		log.Fatal("Owner: ", err)
	}
	println("Owner", ownerAddress.Hex())

	summaInit, err := seS.Contributions(common.HexToAddress("0x58247697e29520762c94aFAa3294F1eC12624BCf"))
	if err != nil {
		log.Fatal("Contributions: ", err)
	}
	fmt.Println("Сумма инициатора", summaInit)

	summaVz, err := seS.Contributions(common.HexToAddress("0x065787057b61eC24ec7AA6B31433A29DBd1C5908"))
	if err != nil {
		log.Fatal("Contributions: ", err)
	}
	fmt.Println("Сумма взлмщика", summaVz)

	ContrbSumm, err := seS.GetContribution()
	if err != nil {
		log.Fatal("GetContribution: ", err)
	}
	fmt.Println("Сумма текущего аккаунта", ContrbSumm)

	// --- КОНЕЦ работы с элементами seS !!!

	println("Работа с СмК прошла успешно!")
	return nil
}
