package base_template

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/suite"
	"gitlab.com/aklin/daowarm/bindings"
	infr "gitlab.com/aklin/daowarm/cmd/dao-start/infrastructure"
	"gitlab.com/aklin/linkch/accounts/abi/bind"
	"gitlab.com/aklin/linkch/accounts/abi/bind/backends"
	"gitlab.com/aklin/linkch/common"
	"gitlab.com/aklin/linkch/core"
	"gitlab.com/aklin/linkch/crypto"
	"math/big"
	"os"
	"testing"
)

// Можно запустить из папки dao-start (но только здесь, в терминале, из-под среды):
// clear && go test ./dao/base_template/ -v -timeout 20m
// А можно запускать из RUN

func simTestBackend(testAddr common.Address) *backends.SimulatedBackend {
	return backends.NewSimulatedBackend(
		core.GenesisAlloc{
			testAddr: {Balance: big.NewInt(10000000000000000)},
		}, 10000000,
	)
}

type MainHelper struct {
	suite.Suite
	sim           *backends.SimulatedBackend
	rootAuth      *bind.TransactOpts
	myProxyAsTest *bindings.MyProxyTest
}

func TestMain(m *testing.M) {
	// Здесь можно сделать инциализацию
	// Запуск тестов и выход
	os.Exit(m.Run())
}

func Test_InitTestTemplate(t *testing.T) {
	s := MainHelper{}
	suite.Run(t, &s)
	s.sim.Close()
	fmt.Println("\n***** Конец тестов")
}

func (s *MainHelper) Test_000000_TestTemplate() {
	testKey, err := crypto.GenerateKey()
	s.Empty(err)
	rootAddr := crypto.PubkeyToAddress(testKey.PublicKey)
	s.sim = simTestBackend(rootAddr)
	bgCtx := context.Background()
	code, err := s.sim.CodeAt(bgCtx, rootAddr, nil)
	s.Empty(err)
	s.Empty(code)

	s.rootAuth, err = bind.NewKeyedTransactorWithChainID(testKey, big.NewInt(1337))
	s.Empty(err)

	myProxyAddr, tx, myProxy, err := bindings.DeployMyProxy(s.rootAuth, s.sim)
	s.Empty(err)
	fmt.Println("DeployMyProxy: Потрачено газа", infr.WeiToGwei(tx.Cost()))
	code, err = s.sim.PendingCodeAt(bgCtx, myProxyAddr)
	s.Empty(err)
	s.NotEmpty(code)
	s.sim.Commit()

	myProxyTestAddr, tx, _, err := bindings.DeployMyProxyTest(s.rootAuth, s.sim)
	s.Empty(err)
	fmt.Println("DeployMyProxyTest: Потрачено газа", infr.WeiToGwei(tx.Cost()))
	code, err = s.sim.PendingCodeAt(bgCtx, myProxyTestAddr)
	s.Empty(err)
	s.NotEmpty(code)
	s.sim.Commit()

	tx, err = myProxy.SetImplementation(s.rootAuth, myProxyTestAddr)
	s.Empty(err)
	fmt.Println("SetImplementation: Потрачено газа", infr.WeiToGwei(tx.Cost()))
	s.sim.Commit()

	s.myProxyAsTest, err = bindings.NewMyProxyTest(myProxyAddr, s.sim)
	s.Empty(err)
	expectedVersion := "0.1"
	tx, err = s.myProxyAsTest.SetVersion(s.rootAuth, expectedVersion)
	s.Empty(err)
	fmt.Println("SetVersion: Потрачено газа", infr.WeiToGwei(tx.Cost()))
	s.sim.Commit()

	actualVersion, err := s.myProxyAsTest.Version(&bind.CallOpts{From: s.rootAuth.From})
	s.Empty(err)
	s.Equal(expectedVersion, actualVersion)
}

func (s *MainHelper) Test_000001_TestTemplate() {
	channel := make(chan *bindings.MyProxyTestItemSet, 1)
	sub, err := s.myProxyAsTest.WatchItemSet(&bind.WatchOpts{}, channel)
	s.Empty(err)
	defer sub.Unsubscribe()

	key01 := infr.B32("key_01")
	value01 := infr.B32("Значение ключа 01")
	descr01 := infr.B32("Описание ключа 01")
	tx, err := s.myProxyAsTest.SetItem(s.rootAuth, key01, bindings.MyTestData{key01, value01, descr01})
	s.Empty(err)
	fmt.Println("SetItem: Потрачено газа", infr.WeiToGwei(tx.Cost()))
	s.sim.Commit()

	event := <-channel
	s.Equal(key01, event.Key)
	s.Equal(key01, event.Data.Value)
	s.Equal(value01, event.Data.DataType)
	s.Equal(descr01, event.Data.DataDescr)

	result01, err := s.myProxyAsTest.Items(&bind.CallOpts{From: s.rootAuth.From}, key01)
	s.Equal(key01, result01.Value)
	s.Equal(value01, result01.DataType)
	s.Equal(descr01, result01.DataDescr)
}
