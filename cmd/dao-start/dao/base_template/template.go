package base_template

import (
	"fmt"
	"github.com/urfave/cli/v2"
	"gitlab.com/aklin/daowarm/bindings"
	infr "gitlab.com/aklin/daowarm/cmd/dao-start/infrastructure"
	"gitlab.com/aklin/linkch/common"
	"log"
)

var (
	startFlag = cli.StringFlag{
		Name:  "start-flag",
		Usage: "Просто какой-то флаг...",
	}

	// --- Задать функцию вызова !!!
	Template = cli.Command{
		// --- Задать параметр вызова !!!
		Name:   "base_template",
		Usage:  "Проба пера...",
		Action: start,
		Flags:  []cli.Flag{&startFlag},
	}
)

func buildSession(ctx *cli.Context) (
	// --- Задать объект сессии [[СмК]]Session, меняем его, а дальше просто исправляем ошибки и ВСЕ !!!
	seS *bindings.MyProxyTestSession) {
	fmt.Println("Привязываемся к клиенту...")
	c, err := infr.NewClientWithContext(ctx)
	if err != nil {
		log.Fatal("NewClientWithContext: ", err)
	}
	if false {
		fmt.Println("Деплоим СмК...")
		// --- Задать функцию деплоя Deploy[[СмК]]Sync !!!
		_, _, seS, err = bindings.DeployMyProxyTestSync(c.TxSession, c.ETHClient) // --- Задать входные параметры деплоя !!!
	} else {
		fmt.Println("Привязываемся к СмК...")
		// --- Задать функцию привязки New[[СмК]]Session !!!
		seS, err = bindings.NewMyProxyTestSession(
			common.HexToAddress("0x8340Bca7383471B3293e663C9b1d024c6f59e868"),
			c.ETHClient, c.TxSession)
	}
	if err != nil {
		log.Fatal("NewStoreSession: ", err)
	}
	fmt.Println("Адрес контракта", seS.Address.Hex())
	return seS
}

func start(ctx *cli.Context) error {
	seS := buildSession(ctx)

	// --- Работа с элементами seS !!!

	//_, _, err := seS.SetVersion("0.2")
	//if err != nil {
	//	log.Fatal("Version: ", err)
	//}
	version, err := seS.Version()
	if err != nil {
		log.Fatal("Version: ", err)
	}
	fmt.Println("Версия контракта: ", version)

	// Первый мэпинг
	key01 := infr.B32("key_05")
	//value01 := infr.B32("Значение ключа 05")
	//descr01 := infr.B32("Описание ключа 05")
	//_, _, err = seS.SetItem(key01, bindings.MyTestData{key01, value01, descr01})
	//if err != nil {
	//	log.Fatal("SetItem: ", err)
	//}
	key02 := infr.B32("key_06")
	//value02 := infr.B32("Значение ключа 06")
	//descr02 := infr.B32("Описание ключа 06")
	//_, _, err = seS.SetItem(key02, bindings.MyTestData{key02, value02, descr02})
	//if err != nil {
	//	log.Fatal("SetItem: ", err)
	//}
	result01, err := seS.Items(key01)
	if err != nil {
		log.Fatal("Items: ", err)
	}
	fmt.Println(fmt.Sprintf("value: %s, dataType: %s, dataDescr: %s",
		infr.ToString(result01.Value),
		infr.ToString(result01.DataType),
		infr.ToString(result01.DataDescr)),
	)
	result02, err := seS.Items(key02)
	if err != nil {
		log.Fatal("Items: ", err)
	}
	fmt.Println(fmt.Sprintf("value: %s, dataType: %s, dataDescr: %s",
		infr.ToString(result02.Value),
		infr.ToString(result02.DataType),
		infr.ToString(result02.DataDescr)),
	)

	// Второй мэпинг
	key21 := infr.B32("key_23")
	//value21 := infr.B32("Значение ключа 23")
	//_, _, err = seS.SetItem2(key21, bindings.MyTestData2{key21, value21})
	//if err != nil {
	//	log.Fatal("SetItem2: ", err)
	//}
	key22 := infr.B32("key_24")
	//value22 := infr.B32("Значение ключа 24")
	//_, _, err = seS.SetItem2(key22, bindings.MyTestData2{key22, value22})
	//if err != nil {
	//	log.Fatal("SetItem2: ", err)
	//}
	result21, err := seS.Items2(key21)
	if err != nil {
		log.Fatal("Items2: ", err)
	}
	fmt.Println(fmt.Sprintf("value: %s, dataType: %s",
		infr.ToString(result21.Value),
		infr.ToString(result21.DataType)),
	)
	result22, err := seS.Items2(key22)
	if err != nil {
		log.Fatal("Items2: ", err)
	}
	fmt.Println(fmt.Sprintf("value: %s, dataType: %s",
		infr.ToString(result22.Value),
		infr.ToString(result22.DataType)),
	)

	// --- КОНЕЦ работы с элементами seS !!!

	println("Работа с СмК прошла успешно!")
	return nil
}
