package configs

type ClientConfig struct {
	ETHURL   string `json:",omitempty"`
	ChainID  int    `json:",omitempty"`
	KeyStore string `json:",omitempty"`
}
