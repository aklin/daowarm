package utils

import (
	"github.com/urfave/cli/v2"
)

var (
	ConfigPath string
	cliConfig  = Config{}

	ConfigFlag = cli.StringFlag{
		Name:        "config, cfg, c",
		Value:       "config.toml",
		Usage:       "config file path",
		Destination: &ConfigPath,
	}
	ETHURL = cli.StringFlag{
		Name:        "ethUrl",
		Value:       "",
		Usage:       "Geth API URL",
		Destination: &cliConfig.ETHURL,
	}
	ChainID = cli.IntFlag{
		Name:        "chainId",
		Value:       8443,
		Usage:       "ChainID",
		Destination: &cliConfig.ChainID,
	}
	KeyStore = cli.StringFlag{
		Name:        "keyStore",
		Value:       "file:dds?mode=memory&api=shared",
		Usage:       "KeyStore",
		Destination: &cliConfig.KeyStore,
	}

	GeneralFlags = []cli.Flag{
		&ConfigFlag,
		&ETHURL,
		&ChainID,
		&KeyStore,
	}
)
