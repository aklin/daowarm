Папка aklin@Rotor:~/go/src/git.fintechru.org/masterchain/DAO-Projects/go-ethereum-data-dir  

geth клонирован из:  
git clone https://gitlab.com/aklin/linkch.git  
Версия go-ethereum, с которого создана эта ветка, 1.11 в ней используется PoW. Это принципиально, в более поздних версиях удален PoW (Proof-of-work) и заменен на PoS (Proof-of-Stake),  
так какие-то сложности в настройке локального запуска майнеров, не было времени разбираться... 1.11 вполне себе подходит и отлично работает с последними версиями solidity (>= 8)  

geth лежит в /home/aklin/work/goDev/linkch/build/bin  
папка вписана в пути ОС:  
export PATH=$PATH:/home/aklin/work/goDev/linkch/build/bin  


geth account new --datadir /home/aklin/go/src/git.fintechru.org/masterchain/DAO-Projects/go-ethereum-data-dir  

Public address of the key:   0xCe28E254bB01B296A1fa73D5a6Ca58caEBB4d944  
Path of the secret key file: /home/aklin/go/src/git.fintechru.org/masterchain/DAO-Projects/go-ethereum-data-dir/keystore/UTC--2023-09-21T11-09-52.200117256Z--ce28e254bb01b296a1fa73d5a6ca58caebb4d944  

Public address of the key:   0x58247697e29520762c94aFAa3294F1eC12624BCf  
Path of the secret key file: /home/aklin/go/src/git.fintechru.org/masterchain/DAO-Projects/go-ethereum-data-dir/keystore/UTC--2023-09-21T11-10-41.688303378Z--58247697e29520762c94afaa3294f1ec12624bcf  

Public address of the key:   0x065787057b61eC24ec7AA6B31433A29DBd1C5908  
Path of the secret key file: /home/aklin/go/src/git.fintechru.org/masterchain/DAO-Projects/go-ethereum-data-dir/keystore/UTC--2023-09-21T11-15-12.645034003Z--065787057b61ec24ec7aa6b31433a29dbd1c5908  


geth --datadir /home/aklin/go/src/git.fintechru.org/masterchain/DAO-Projects/go-ethereum-data-dir init /home/aklin/go/src/git.fintechru.org/masterchain/DAO-Projects/go-ethereum-data-dir/genesis.json  


geth --miner.etherbase Ce28E254bB01B296A1fa73D5a6Ca58caEBB4d944 --mine --networkid 4785 --unlock 0x58247697e29520762c94aFAa3294F1eC12624BCf --password ppp.txt --rpc.enabledeprecatedpersonal --datadir /home/aklin/go/src/git.fintechru.org/masterchain/DAO-Projects/go-ethereum-data-dir  


geth --datadir /home/aklin/go/src/git.fintechru.org/masterchain/DAO-Projects/go-ethereum-data-dir attach /home/aklin/go/src/git.fintechru.org/masterchain/DAO-Projects/go-ethereum-data-dir/geth.ipc  


eth.accounts  
base = eth.accounts[0]  
miner.setEtherbase(base)  
miner.start(1)  

eth.getBalance("0xCe28E254bB01B296A1fa73D5a6Ca58caEBB4d944")  
eth.getBalance("0x58247697e29520762c94aFAa3294F1eC12624BCf")  
eth.getBalance("0x065787057b61eC24ec7AA6B31433A29DBd1C5908")  

personal.unlockAccount( "0xCe28E254bB01B296A1fa73D5a6Ca58caEBB4d944","pwd")  
personal.unlockAccount( "0x58247697e29520762c94aFAa3294F1eC12624BCf","pwd")  
personal.unlockAccount( "0x065787057b61eC24ec7AA6B31433A29DBd1C5908","pwd")  

var sender = "0xCe28E254bB01B296A1fa73D5a6Ca58caEBB4d944"  
var receiver = "0x58247697e29520762c94aFAa3294F1eC12624BCf"  
var amount = web3.toWei(1, "ether")  
eth.sendTransaction({from:sender, to:receiver, value: amount})  

По окончанию сеанса работы с geth, перед закрытием приложения, ОБЯЗАТЕЛЬНО ВЫПОЛНИТЬ miner.stop(), ИНАЧЕ ВСЕ ТРАНЗАКЦИИ СЕАНСА БУДУТ ПОТЕРЯНЫ.  

